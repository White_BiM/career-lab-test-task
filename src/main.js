import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify'
import axios from 'axios'
import VueAxios from 'vue-axios'
import Vuex from 'vuex'
import VueRouter from 'vue-router'
import router from './router'
import store from './store'

Vue.use(VueAxios, axios, Vuex, VueRouter)

Vue.config.productionTip = false

new Vue({
    vuetify,
    router,
    store,
    render: h => h(App),
    created() {
        this.$store.dispatch('getCandidates')
    }
}).$mount('#app')
