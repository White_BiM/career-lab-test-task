import Vue from "vue"
import Vuex from 'vuex'
import candidates from './candidates'
Vue.use(Vuex)

export default new Vuex.Store({
    modules: {
        candidates
    }

})
