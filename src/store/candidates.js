import axios from 'axios'

const proxyurl = 'https://cors-anywhere.herokuapp.com/'
const url = 'https://fakedata.dev/'

const HTTP = axios.create({
    baseURL: proxyurl + url,
})
export default {
    state: {
        loading: false,
        candidates: [],
    },
    mutations: {
        setLoading(state, payload) {
            state.loading = payload
        },
        setCandidates(state, payload){
            state.candidates = payload
        },
    },
    actions: {
        setLoading ({commit}, payload) {
            commit('setLoading', payload)
        },
        async getCandidates({commit}){
            commit('setLoading', true)
            try{
                await HTTP.get('users/v1/get_users').then(response => {
                    commit('setCandidates', response.data)
                    commit('setLoading', false)
                })
            } catch(error){
                commit('setLoading', false)
                throw error
            }
        },
    },

    getters:  {
        loading(state) {
            return state.loading
        },
        candidates (state) {
            return state.candidates
        },
        candidateById (state) {
            return candidateId => {
                return state.candidates.find(candidate => candidate.id === candidateId)
            }
        },
    }
}